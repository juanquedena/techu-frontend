import { axios } from '@bundled-es-modules/axios';
import { store } from '../redux/store.js';
import { getError } from '../redux/actionsCreators/error-actions.js';
import { GET_LOADING_ON, GET_LOADING_OFF } from '../redux/actions.js';

const { API_URL } = process.env;

const getLoadingOn = { type: GET_LOADING_ON };
const getLoadingOff = { type: GET_LOADING_OFF };

const config = {
  withCredentials: false,
  timeout: 10000,
  baseURL: API_URL,
  headers: {
    'Cache-Control': 'no-cache, no-store, must-revalidate',
    'Content-Type': 'application/json',
    Pragma: 'no-cache',
  },
};

export default class RestClient {
  constructor() {
    this.axiosClient = axios.create(config);

    this.axiosClient.interceptors.request.use(
      configuration => {
        store.dispatch(getLoadingOn);
        const configTemp = { ...configuration };
        configTemp.headers['signature-issuer'] = 'signature-issuer-dummy';
        configTemp.headers['signature-subject'] = 'signature-subject-dummy';
        const { user } = store.getState().app;
        if (user.authorization !== undefined) {
          configTemp.headers.Authorization = `Bearer ${user.authorization.accessToken}`;
          configTemp.headers['token-refresh'] = user.authorization.refreshToken;
        }
        return configTemp;
      },
      error => {
        store.dispatch(getLoadingOff);
        return Promise.reject(error);
      }
    );

    this.axiosClient.interceptors.response.use(
      response => {
        store.dispatch(getLoadingOff);
        return response;
      },
      error => {
        if (error.response) {
          store.dispatch(getError(error.response));
        } else {
          const response = {
            data: {
              messages: [{ msg: error.message }],
            },
          };
          store.dispatch(getError(response));
        }
        return Promise.reject(error);
      }
    );
  }

  async post(path, data) {
    // const payload = Buffer.from(JSON.stringify(data)).toString('base64');
    const payload = JSON.stringify(data);
    return this.axiosClient
      .post(path, payload, config)
      .then(response => response);
  }

  async patch(path, data) {
    const payload = JSON.stringify(data);
    return this.axiosClient
      .patch(path, payload, config)
      .then(response => response);
  }

  async put(path, data) {
    const payload = JSON.stringify(data);
    return this.axiosClient
      .put(path, payload, config)
      .then(response => response);
  }

  async get(path) {
    return this.axiosClient.get(path, config).then(response => response);
  }
}
