import { LitElement, html, css } from 'lit-element';
import { router } from 'lit-element-router';
import { connect } from 'pwa-helpers';
import { store } from './redux/store.js';
import { getCuentas } from './redux/actionsCreators/cuentas-actions.js';
import { getMovimientos } from './redux/actionsCreators/movimientos-actions.js';
import '@material/mwc-linear-progress';
import './components/app-main.js';
import './components/app-toolbar.js';
import './components/app-dialog-error.js';
import './pages/persona-page.js';
import './pages/empresa-page.js';
import './pages/login-page.js';
import './pages/user-register-page.js';
import './pages/cuentas-page.js';
import './pages/movimientos-page.js';
import './pages/movimiento-formulario.js';

const authenticate = {
  authentication: {
    unauthenticated: {
      name: 'iniciar-sesion',
    },
    authenticate: () => {
      return store.getState().app.login;
    },
  },
};

export class BankFront extends connect(store)(router(LitElement)) {
  static get properties() {
    return {
      route: { type: String },
      params: { type: Object },
      query: { type: Object },
      data: { type: Object },
      loading: { type: Boolean },
    };
  }

  static get routes() {
    return [
      {
        name: 'personas',
        pattern: 'personas',
        data: { title: 'Home' },
      },
      {
        name: 'empresas',
        pattern: 'empresas',
      },
      {
        name: 'iniciar-sesion',
        pattern: 'iniciar-sesion',
      },
      {
        name: 'crear-cuenta',
        pattern: 'crear-cuenta',
      },
      {
        name: 'cuentas',
        pattern: 'cuentas',
        ...authenticate,
      },
      {
        name: 'movimientos',
        pattern: 'movimientos',
        ...authenticate,
      },
      {
        name: 'movimiento-formulario',
        pattern: 'movimiento-formulario',
        ...authenticate,
      },
      {
        name: 'not-found',
        pattern: '*',
      },
    ];
  }

  constructor() {
    super();
    this.route = '';
    this.params = {};
    this.query = {};
    this.loading = true;
  }

  router(route, params, query, data) {
    this.route = route;
    this.params = params;
    this.query = query;
    this.data = data;
    const { app } = store.getState();
    if (app.login) {
      const { userId } = app.user.data;
      switch (this.route) {
        case 'cuentas':
          store.dispatch(getCuentas(userId));
          break;
        case 'movimientos':
          store.dispatch(getMovimientos(userId, query.cuenta));
          break;
        default:
      }
    }
    // console.log(route, params, query, data);
  }

  static get styles() {
    return css`
      :host {
        min-height: 100vh;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-start;
        font-size: calc(10px + 2vmin);
        color: #1a2b42;
        max-width: 960px;
        margin: 0 auto;
        text-align: center;
      }

      main {
        flex-grow: 1;
      }

      mwc-linear-progress {
        --mdc-theme-primary: #0e71ba;
      }
    `;
  }

  render() {
    return html`
      <main>
        <app-toolbar></app-toolbar>
        <mwc-linear-progress
          indeterminate
          .closed="${this.loading}"
        ></mwc-linear-progress>
        <app-main active-route=${this.route}>
          <persona-page route="personas"></persona-page>
          <empresa-page route="empresas"></empresa-page>
          <login-page route="iniciar-sesion"></login-page>
          <user-register-page route="crear-cuenta"></user-register-page>
          <cuentas-page route="cuentas"></cuentas-page>
          <movimientos-page
            route="movimientos"
            cuenta="${this.query.cuenta}"
          ></movimientos-page>
          <movimiento-formulario
            route="movimiento-formulario"
            cuenta="${this.query.cuenta}"
          ></movimiento-formulario>
          <h1 route="not-found">Not Found</h1>
        </app-main>
        <app-dialog-error></app-dialog-error>
      </main>
    `;
  }

  stateChanged(state) {
    this.loading = state.app.loading;
  }
}

customElements.define('bank-front', BankFront);
