import { POST_USER } from '../actions.js';
import RestClient from '../../utils/rest-client.js';

export const crearCuenta = (firstName, lastName,email,password,password2) => dispatch => {
  const restClient = new RestClient();
  restClient.post(`users`, { firstName, lastName,email,password,password2}).then(response => {
    return dispatch({
      type: POST_USER,
      data: response.data,
    });
  });

        

};
