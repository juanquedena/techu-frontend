import { GET_ERROR } from '../actions.js';

export const getError = response => dispatch => {
  return dispatch({
    type: GET_ERROR,
    error: response,
  });
};
