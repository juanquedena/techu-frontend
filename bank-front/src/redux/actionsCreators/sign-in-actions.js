import { GET_SIGN_IN } from '../actions.js';
import RestClient from '../../utils/rest-client.js';

export const getSignIn = (username, password) => dispatch => {
  const restClient = new RestClient();
  restClient.post('secure/signIn', { username, password }).then(response => {
    return dispatch({
      type: GET_SIGN_IN,
      user: response.data,
      login: true,
    });
  });
};
