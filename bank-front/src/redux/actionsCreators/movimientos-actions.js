import { GET_ALL_TRANSACTION, GET_TRANSACTION,GET_ACCOUNTS,POST_TRANSACTION} from '../actions.js';
import RestClient from '../../utils/rest-client.js';

export const getMovimientos = (accountId, userId) => dispatch => {
  const restClient = new RestClient();
  restClient
    .get(`transactions?query={accountId:${accountId},userId:${userId}}`)
    .then(response => {
      return dispatch({
        type: GET_ALL_TRANSACTION,
        transactions: response.data.data,
      });
    });
};

export const getMovimiento = transactionId => dispatch => {
  const restClient = new RestClient();
  restClient.get(`transactions/${transactionId}}`).then(response => {
    return dispatch({
      type: GET_TRANSACTION,
      transaction: response.data.data,
    });
  });
};

export const registrarMovimiento = (description, userId,accountId,amount,type,userReceiveId,accountReceiveId) => dispatch => {
  const restClient = new RestClient();
  restClient.post('transactions', { description, userId,accountId,amount,type,userReceiveId,accountReceiveId }).then(response => {
      return dispatch({
        type: POST_TRANSACTION,
        transactions: response.data.data,
      });
    });
};

export const obtenerCuenta = iban => dispatch => {
  const restClient = new RestClient();
  restClient.get(`accounts?iban=${iban}`).then(response => {
      return dispatch({
        type: GET_ACCOUNTS,
        account: response.data.data,
      });
    });
};

