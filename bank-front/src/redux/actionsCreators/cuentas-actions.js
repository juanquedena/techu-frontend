import { GET_ALL_ACCOUNTS } from '../actions.js';
import RestClient from '../../utils/rest-client.js';

export const getCuentas = idUser => dispatch => {
  const restClient = new RestClient();
  restClient.get(`users/${idUser}/?expands=accounts`).then(response => {
    return dispatch({
      type: GET_ALL_ACCOUNTS,
      accounts: response.data.data.accounts,
    });
  });
};
