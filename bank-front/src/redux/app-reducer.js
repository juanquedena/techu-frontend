import {
  GET_SIGN_IN,
  GET_ERROR,
  GET_LOADING_ON,
  GET_LOADING_OFF,
  GET_ALL_ACCOUNTS,
  GET_ALL_TRANSACTION,
  GET_TRANSACTION,
  GET_ACCOUNTS
} from './actions.js';

const INITIAL_STATE = {
  user: {},
  accounts: [],
  account: { user: {} },
  loading: true,
  login: false,
};

const app = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_SIGN_IN:
      return {
        ...state,
        user: action.user,
        login: action.login,
      };
    case GET_ERROR:
      return {
        ...state,
        error: action.error,
        loading: true,
      };
    case GET_LOADING_ON:
      return {
        ...state,
        loading: false,
      };
    case GET_LOADING_OFF:
      return {
        ...state,
        loading: true,
      };
    case GET_ALL_ACCOUNTS:
      return {
        ...state,
        accounts: action.accounts,
      };
    case GET_ALL_TRANSACTION:
      return {
        ...state,
        transactions: action.transactions,
      };
    case GET_TRANSACTION:
      return {
        ...state,
        transaction: action.transaction,
      };
    case GET_ACCOUNTS:
      return {
        ...state,
        account: action.account,
    };

    default:
      return state;
  }
};

export default app;
