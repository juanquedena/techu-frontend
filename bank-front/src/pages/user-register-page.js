import { LitElement, html, css } from 'lit-element';
import '../components/app-link.js';
import { navigator } from 'lit-element-router';
import { crearCuenta } from '../redux/actionsCreators/user-register-actions.js';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { sharedStyles } from '../components/shared-styles.js';

export class UserRegisterPage extends connect(store)(navigator(LitElement)) {

  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
      firstName: { type: String },
      lastName: { type: String },
      email: { type: String },
      password: { type: String },
      password2: { type: String }
    };
  }

  static get styles() {
    return [
      sharedStyles,
      css`
        :host {
          display: inline-block;
        }
      `,
    ];
  }

  crearCuenta() {
    store.dispatch(crearCuenta(
        this.firstName,
        this.lastName,
        this.email,
        this.password,
        this.password2));
  }


  clear() {
        this.firstName="";
        this.lastName="";
        this.email="";
        this.password="";
        this.password2="";
  }

  constructor() {
    super();
    this.clear();
  }

  render() {
    return html`
      <main>
        <h1>¡ Cree su cuenta !</h1>
        <div class="row">
          <mwc-textfield
            label="Nombres"
            type="text"
            .value="${this.firstName}"
            iconTrailing="account_circle"
            required
          ></mwc-textfield>
        </div>
        <div class="row">
          <mwc-textfield
            label="Apellidos"
            type="text"
            .value="${this.lastName}"
            iconTrailing="account_circle"
            required
          ></mwc-textfield>
        </div>
        <div class="row">
          <mwc-textfield
            label="Correo electrónico"
            type="email"
            .value="${this.email}"
            iconTrailing="email"
            required
          ></mwc-textfield>
        </div>
        <div class="row">
          <mwc-textfield
            label="Contraseña"
            type="password"
            .value="${this.password}"
            iconTrailing="visibility_off"
            required
          ></mwc-textfield>
        </div>
        <div class="row">
          <mwc-textfield
            label="Confirmar Contraseña"
            type="password"
            .value="${this.password2}"
            iconTrailing="visibility_off"
            required
          ></mwc-textfield>
        </div>
        <div class="row float-right m-t--8">
          <app-button class="m-r--5" @clicked="${this.crearCuenta}"
          >Crear</app-button>
          <app-button-link secondary="true" href="/personas"
            >Cancelar</app-button-link
          >
        </div>
      </main>
    `;
  }
}

customElements.define('user-register-page', UserRegisterPage);
