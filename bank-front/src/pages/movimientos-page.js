import { LitElement, html, css } from 'lit-element';
import { connect } from 'pwa-helpers';
import { navigator } from 'lit-element-router';
import { store } from '../redux/store.js';
import { sharedStyles } from '../components/shared-styles.js';
import '@vaadin/vaadin-grid';

class MovimientosPage extends connect(store)(navigator(LitElement)) {
  static get properties() {
    return {
      cuenta: { type: String },
      movimientos: { type: String, attribute: false },
    };
  }

  constructor() {
    super();
    this.movimientos = [];
  }

  static get styles() {
    return [sharedStyles, css``];
  }

  render() {
    return html`
      <vaadin-grid .items="${this.movimientos}">
        <vaadin-grid-column
          path="description"
          header="Descripción"
        ></vaadin-grid-column>
        <vaadin-grid-column path="date" header="Fecha"></vaadin-grid-column>
        <vaadin-grid-column
          path="amount"
          header="Monto"
          text-align="end"
          width="120px"
          flex-grow="0"
        ></vaadin-grid-column>
      </vaadin-grid>
      <app-button class="m-r--5" @clicked="${this.registrarMovimiento}"
        >Registrar Movimiento</app-button
      >
    `;
  }

  stateChanged(state) {
    this.movimientos = state.app.transactions;
  }

  registrarMovimiento() {
    this.navigate(`/movimiento-formulario?cuenta=${this.cuenta}`);
  }
}

customElements.define('movimientos-page', MovimientosPage);
