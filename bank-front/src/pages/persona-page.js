import { LitElement, html, css } from 'lit-element';

export class PersonaPage extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
    };
  }

  static get styles() {
    return css``;
  }

  render() {
    return html`
      <div>
        <h1>Personas</h1>
      </div>
    `;
  }
}

customElements.define('persona-page', PersonaPage);
