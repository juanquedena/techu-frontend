import { LitElement, html, css } from 'lit-element';
import { navigator } from 'lit-element-router';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { getSignIn } from '../redux/actionsCreators/sign-in-actions.js';
import { sharedStyles } from '../components/shared-styles.js';
import '@material/mwc-textfield';
import '../components/app-button.js';

export class LoginPage extends connect(store)(navigator(LitElement)) {
  static get properties() {
    return {
      username: { type: String },
      password: { type: String },
      login: { type: Boolean, attribute: false },
    };
  }

  static get styles() {
    return [
      sharedStyles,
      css`
        :host {
          display: inline-block;
          width: 450px;
        }
      `,
    ];
  }

  constructor() {
    super();
    this.login = false;
    this.clear();
  }

  render() {
    return html`
      <main>
        <h1>¡ Bienvenido !</h1>
        <div class="row">
          <mwc-textfield
            id="username"
            .value="${this.username}"
            label="Nombre de usuario"
            type="text"
            iconTrailing="account_circle"
            @change="${this.changeValue}"
          ></mwc-textfield>
        </div>
        <div class="row">
          <mwc-textfield
            id="password"
            .value="${this.password}"
            label="Contraseña"
            type="password"
            iconTrailing="visibility_off"
            @change="${this.changeValue}"
          ></mwc-textfield>
        </div>
        <div class="row float-right m-t--8">
          <app-button class="m-r--5" @clicked="${this.signIn}"
            >Ingresar</app-button
          >
          <app-button-link secondary="true" href="/personas"
            >Cancelar</app-button-link
          >
        </div>
      </main>
    `;
  }

  signIn() {
    store.dispatch(getSignIn(this.username, this.password));
  }

  clear() {
    this.username = 'isemmence0@google.nl';
    this.password = 'VxlKwL';
  }

  changeValue(e) {
    this[e.target.id] = e.target.value;
  }

  stateChanged(state) {
    this.login = state.app.login;
  }

  shouldUpdate(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      // console.log(`${propName} changed. oldValue: ${oldValue}`);
      if (propName === 'login' && this.login) {
        this.navigate('/cuentas');
      }
    });
    return true;
  }
}

customElements.define('login-page', LoginPage);
