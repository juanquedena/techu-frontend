import { LitElement, html, css } from 'lit-element';
import { navigator } from 'lit-element-router';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { sharedStyles } from '../components/shared-styles.js';
import '@material/mwc-list';
import '@material/mwc-list/mwc-list-item';
import '@material/mwc-icon/mwc-icon';

export class CuentasPage extends connect(store)(navigator(LitElement)) {
  static get properties() {
    return {
      cuentas: { type: Object },
    };
  }

  static get styles() {
    return [
      sharedStyles,
      css`
        :host {
          display: inline-block;
          width: 900px;
        }
        mwc-icon {
          background-color: gray;
          color: white;
        }
      `,
    ];
  }

  constructor() {
    super();
    this.cuentas = [];
  }

  render() {
    return html`
      <main>
      <div class="row">
      <div class="col-9">
        <div class="producto">
        </div>
        <br/>
        <br/>
        <div class="container">
          <div class="row tabla-titulo">
            Cuentas
          </div>
          <div class="row tabla-titulo">
            <div class="col-sm ">
              TIPO
            </div>
            <div class="col-sm">
              NRO DE CUENTA
            </div>
            <div class="col-sm float-right">
              MONEDA
            </div>
            <div class="col-sm float-right">
              MONTO
            </div>
          </div>
          ${this.cuentas.map(
            item =>
              html`
                <div class="tabla-contenido ">
                  <div class="row">
                    <div class="col-sm text-left">
                      ${item.card.type}
                    </div>
                    <div class="col-sm texto-titulo">
                      <a
                        href="javascript: void(0)"
                        @click="${() => {
                          this.verMovimientos(item);
                        }}"
                      >
                        ${item.card.number}
                      </a>
                    </div>
                    <div class="col-sm text-center">
                      ${item.currencyCode}
                    </div>
                    <div class="col-sm text-right">
                      ${item.balance}
                    </div>
                  </div>
                </div>
              `
          )}
        </div>
        </div>
        <div class="col-3">
            <div class="tecambio">
            </div>
        </div>
      </div>
        
        
      </main>
    `;
  }

  verMovimientos(item) {
    this.navigate(`/movimientos?cuenta=${item.accountId}`);
  }

  stateChanged(state) {
    this.cuentas = state.app.accounts;
  }
}

customElements.define('cuentas-page', CuentasPage);
