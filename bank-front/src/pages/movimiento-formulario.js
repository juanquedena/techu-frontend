import { LitElement, html, css } from 'lit-element';
import { navigator } from 'lit-element-router';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { registrarMovimiento, obtenerCuenta } from '../redux/actionsCreators/movimientos-actions.js';
import { sharedStyles } from '../components/shared-styles.js';
import '@material/mwc-textfield';
import '../components/app-button.js';

export class MovimientoFormulario extends connect(store)(navigator(LitElement)) {

  static get properties() {
    return {
      description       : { type: String },
      amount            : { type: Number },
      type              : { type: String },
      iban              : { type: String },
      beneficiario      : { type: String },
      userRevivedId     : { type: String },
      accountRevivedId  : { type: String },
      userId            : { type: String },
      cuenta            : { type: String }
    };
  }

  static get styles() {
    return [
      sharedStyles,
      css`
        :host {
          display: inline-block;
          width: 450px;
        }
      `,
    ];
  }

  constructor() {
    super();
    this.clear();
  }

  render() {
    return html`
      <main>
        <div class="row">
          <mwc-textfield
            id="iban"
            .value="${this.iban}"
            label="IBAN Destino"
            type="text"
            @change="${this.buscarCuentaBeneficiario}"
          ></mwc-textfield>

          <mwc-textfield
            id="beneficiario"
            value="${this.beneficiario}"
            label="Beneficiario"
            type="text"
            @change="${this.changeValue}"
          ></mwc-textfield>

          <mwc-textfield
            id="description"
            .value="${this.description}"
            label="Descripción"
            type="text"
            @change="${this.changeValue}"
          ></mwc-textfield>
          <mwc-textfield
            id="amount"
            .value="${this.amount}"
            label="Monto"
            type="text"
            @change="${this.changeValue}"
          ></mwc-textfield>

          <app-button class="m-r--5" @clicked="${this.registrar}"
            >Registrar</app-button
          >
        </div>
      </main>
    `;
  }

  buscarCuentaBeneficiario(e){
    store.dispatch(obtenerCuenta(e.target.value));
  }

  stateChanged(state) {
    if(state.app.account.user.firstName && state.app.account.user.lastName) {
      this.beneficiario     = `${state.app.account.user.firstName} ${ state.app.account.user.lastName}`;
      this.userRevivedId    = state.app.account.user.userId;
      this.accountRevivedId = state.app.account.accountId;
      this.userId           = state.app.user.data.userId;
    }
  }

  registrar() {
    store.dispatch(registrarMovimiento(
      this.description,
      this.userId,
      this.cuenta,
      this.amount,
      'SHOULD',
      this.userRevivedId,
      this.accountRevivedId)); 
      this.clear();     
      this.navigate(`/movimientos?cuenta=${this.cuenta}`);
  }

  clear() {
      this.description='';
      this.amount='';
      this.type='';
      this.iban='';
      this.beneficiario='';
      this.userRevivedId='';
      this.accountRevivedId='';
      this.userId='';
  }


  changeValue(e) {
    this[e.target.id] = e.target.value;
  }

}

customElements.define('movimiento-formulario', MovimientoFormulario);
