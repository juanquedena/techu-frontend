import { LitElement, html, css } from 'lit-element';

export class EmpresaPage extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
    };
  }

  static get styles() {
    return css``;
  }

  render() {
    return html`
      <main>
        <h1>Empresa</h1>
      </main>
    `;
  }
}

customElements.define('empresa-page', EmpresaPage);
