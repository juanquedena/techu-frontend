import { LitElement, html, css } from 'lit-element';
import { connect } from 'pwa-helpers';
import { store } from '../redux/store.js';
import { GET_ERROR } from '../redux/actions.js';
import { sharedStyles } from './shared-styles.js';
import '@material/mwc-dialog';
import './app-button.js';

class AppDialogError extends connect(store)(LitElement) {
  static get properties() {
    return {
      messages: { type: Object },
    };
  }

  static get styles() {
    return [
      sharedStyles,
      css`
        :host {
          display: block;
        }
        div {
          display: inline-block;
          margin: auto;
        }
      `,
    ];
  }

  render() {
    return this.messages
      ? html`
          <mwc-dialog open @closed="${this.closed}">
            <div>
              <ul>
                ${this.messages.map(
                  (item, index) => html`<li>[${index}]:${item.msg}</li>`
                )}
              </ul>
            </div>
            <app-button slot="primaryAction" dialogAction="close">
              Aceptar
            </app-button>
          </mwc-dialog>
        `
      : '';
  }

  closed() {
    this.messages = undefined;
    store.dispatch({ type: GET_ERROR, error: undefined });
  }

  stateChanged(state) {
    if (state.app.error !== undefined && state.app.error.data !== undefined) {
      if (state.app.error.data.code === 'V01') {
        this.messages = state.app.error.data.messages;
      } else {
        this.messages = [{ msg: state.app.error.data.message }];
      }
    }
  }
}

customElements.define('app-dialog-error', AppDialogError);
