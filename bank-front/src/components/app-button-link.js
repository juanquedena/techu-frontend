import { LitElement, html, css } from 'lit-element';
import { navigator } from 'lit-element-router';

export class AppButtonLink extends navigator(LitElement) {
  static get properties() {
    return {
      href: { type: String },
      secondary: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        display: inline-block;
      }
      app-button {
        --button-height: var(--button-link-height);
      }
    `;
  }

  constructor() {
    super();
    this.href = '';
    this.secondary = '';
  }

  render() {
    return html`
      <app-button
        secondary="${this.secondary}"
        href="${this.href}"
        @click="${this.linkClick}"
      >
        <slot></slot>
      </app-button>
    `;
  }

  linkClick(event) {
    event.preventDefault();
    this.navigate(this.href);
  }
}

customElements.define('app-button-link', AppButtonLink);
