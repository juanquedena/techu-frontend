import { css } from 'lit-element';

/*!
 * Bootstrap v4.5.0 (https://getbootstrap.com/)
 * Copyright 2011-2020 The Bootstrap Authors
 * Copyright 2011-2020 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

export const sharedStyles = css`
  * {
    font-family: Arial, Tahoma;
  }
  .m-r--5 {
    margin-right: 0.5em;
  }
  .m-r-1 {
    margin-right: 1em;
  }
  .m-r-2 {
    margin-right: 2em;
  }
  .m-t--8 {
    margin-top: 0.8em;
  }
  .row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    padding-top: 15px;
    padding-right: 15px;
    padding-left: 15px;
    padding-bottom: 15px;
  }
  .float-right {
    float: right !important;
  }
  mwc-textfield {
    --mdc-theme-primary: #004283;
    width: 100%;
  }

  .col-sm {
    -ms-flex-preferred-size: 0;
    flex-basis: 0;
    -ms-flex-positive: 1;
    flex-grow: 1;
    min-width: 0;
    max-width: 100%;
  }


  .col-9 {
    flex: 0 0 75%;
    width: 75%;
  }

  .col-3 {
    width: 15%;
  }

  .container {
    width: 100%;
    padding-top: 15px;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    font-size: 0.6em;
}


.text-right {
  text-align: right !important;
}
.text-left {
  text-align: left !important;
}
.text-center {
  text-align: center !important;
}
.texto-titulo {
  font-size: 12px;
  font-weight: bold;
  color: #0065b7;
}

.tabla-titulo{
  font-weight: bold;
  background-color: #f8f8f5;
  border: 1px solid #dcdcdc;
  color: #434a58;
  text-transform: uppercase;
}
.tabla-contenido{
  border-bottom: 1px solid #dcdcdc;
}

.tabla-contenido :hover {
    background: #d6f4fc;
}

.producto {
  display: inline-block;
  background: url(../../assets/images/productos.png) no-repeat;
  cursor: pointer;
  width:100%;
  height:230px;
}
.tecambio {
  display: inline-block;
  position: relative;
  background: url(../../assets/images/tcambio.png) no-repeat;
  background-size: contain;
  cursor: pointer;
  padding-right: 25px;
  width:250px;
  height:100%;
}

`;
