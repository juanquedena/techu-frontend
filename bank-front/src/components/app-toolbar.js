import { LitElement, html, css } from 'lit-element';
import { sharedStyles } from './shared-styles.js';
import './app-link.js';
import './app-button-link.js';

export class AppToolbar extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
    };
  }

  static get styles() {
    return [
      sharedStyles,
      css`
        :host {
          display: block;
          /*
          position: fixed;
          top: 0;
          left: 0;
           */
          width: 100vw;
          z-index: 1000;
          background-color: #072146; /* #004283 */
        }
        div.container-navbar {
          padding: 15px 0px;
        }
        ul.navbar {
          width: 1024px;
          list-style-type: none;
          margin: auto;
          padding: 0px;
          overflow: hidden;
          background-color: #072146; /* #004283 */
        }
        ul.navbar li {
          float: left;
        }
        ul.navbar li app-link {
          --theme-link-color: white;
          --theme-link-hover-color: #fff;
          --theme-link-hover-background: #0e71ba;
        }
        ul.navbar li app-link.add-user {
          --theme-link-color: #f8cd51;
          --theme-link-hover-color: #fff;
          --theme-link-hover-background: #0e71ba; /* #004283 */
        }
        ul.navbar li.right {
          float: right;
        }
        .logo {
          display: inline-block;
          position: relative;
          top: 4px;
          left: 15px;
          width: 185px;
          height: 40px;
          background: url(../../assets/images/bac4d364a5245f7395b87ff8562e84a6.png)
            no-repeat;
          background-size: contain;
          cursor: pointer;
          padding-right: 25px;
        }
        
        app-button-link {
          --button-link-height: 45px;
          margin-left: 5px;
          margin-right: 5px;
        }
      `,
    ];
  }

  render() {
    return html`
      <div class="container-navbar">
        <ul class="navbar">
          <li><div class="logo"></div></li>
          <li><app-link href="/personas">Personas</app-link></li>
          <li><app-link href="/empresas">Empresas</app-link></li>
          <li class="right">
            <app-link href="/canales">Canales de Atención</app-link>
          </li>
          <li class="right">
            <app-link href="/buscar" icon="vaadin:search"></app-link>
          </li>
          <li class="right">
            <app-button-link href="/iniciar-sesion"
              >Banca por Internet</app-button-link
            >
          </li>
          <li class="right">
            <app-link
              class="add-user"
              href="/crear-cuenta"
              icon="social:person-add"
              >Crear cuenta</app-link
            >
          </li>
        </ul>
      </div>
    `;
  }
}

customElements.define('app-toolbar', AppToolbar);
