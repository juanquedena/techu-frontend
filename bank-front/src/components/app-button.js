import { LitElement, html, css } from 'lit-element';

class AppButton extends LitElement {
  static get properties() {
    return {
      secondary: { type: String },
      disabledButton: { type: Boolean },
    };
  }

  static get styles() {
    return [
      css`
        :host {
          display: inline-block;
        }
        .btn {
          height: var(--button-height);
          display: inline-block;
          font-weight: 400;
          color: #212529;
          text-align: center;
          vertical-align: middle;
          cursor: pointer;
          -webkit-user-select: none;
          -moz-user-select: none;
          -ms-user-select: none;
          user-select: none;
          background-color: transparent;
          border: 1px solid transparent;
          padding: 0.375rem 0.75rem;
          font-size: 1rem;
          line-height: 1.5;
          border-radius: 0.25rem;
          transition: color 0.15s ease-in-out,
            background-color 0.15s ease-in-out, border-color 0.15s ease-in-out,
            box-shadow 0.15s ease-in-out;
        }
        @media (prefers-reduced-motion: reduce) {
          .btn {
            transition: none;
          }
        }
        .btn:hover {
          color: #212529;
          text-decoration: none;
        }
        .btn:focus,
        .btn.focus {
          outline: 0;
          box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
        }
        .btn.disabled,
        .btn:disabled {
          opacity: 0.65;
          cursor: not-allowed;
        }
        .btn-primary {
          color: #fff;
          background-color: #007bff;
          border-color: #007bff;
        }
        .btn-primary:hover {
          color: #fff;
          background-color: #0069d9;
          border-color: #0062cc;
        }
        .btn-primary:focus,
        .btn-primary.focus {
          color: #fff;
          background-color: #0069d9;
          border-color: #0062cc;
          box-shadow: 0 0 0 0.2rem rgba(38, 143, 255, 0.5);
        }
        .btn-primary.disabled,
        .btn-primary:disabled {
          color: #fff;
          background-color: #007bff;
          border-color: #007bff;
        }
        .btn-primary:not(:disabled):not(.disabled):active,
        .btn-primary:not(:disabled):not(.disabled).active,
        .show > .btn-primary.dropdown-toggle {
          color: #fff;
          background-color: #0062cc;
          border-color: #005cbf;
        }
        .btn-primary:not(:disabled):not(.disabled):active:focus,
        .btn-primary:not(:disabled):not(.disabled).active:focus,
        .show > .btn-primary.dropdown-toggle:focus {
          box-shadow: 0 0 0 0.2rem rgba(38, 143, 255, 0.5);
        }
        .btn-secondary {
          color: #fff;
          background-color: #6c757d;
          border-color: #6c757d;
        }
        .btn-secondary:hover {
          color: #fff;
          background-color: #5a6268;
          border-color: #545b62;
        }
        .btn-secondary:focus,
        .btn-secondary.focus {
          color: #fff;
          background-color: #5a6268;
          border-color: #545b62;
          box-shadow: 0 0 0 0.2rem rgba(130, 138, 145, 0.5);
        }
        .btn-secondary.disabled,
        .btn-secondary:disabled {
          color: #fff;
          background-color: #6c757d;
          border-color: #6c757d;
        }
        .btn-secondary:not(:disabled):not(.disabled):active,
        .btn-secondary:not(:disabled):not(.disabled).active,
        .show > .btn-secondary.dropdown-toggle {
          color: #fff;
          background-color: #545b62;
          border-color: #4e555b;
        }
        .btn-secondary:not(:disabled):not(.disabled):active:focus,
        .btn-secondary:not(:disabled):not(.disabled).active:focus,
        .show > .btn-secondary.dropdown-toggle:focus {
          box-shadow: 0 0 0 0.2rem rgba(130, 138, 145, 0.5);
        }
      `,
    ];
  }

  render() {
    return html`
      <button
        ?disabled="${this.disabledButton}"
        @click="${this.click}"
        class="btn btn-primary
        ${this.secondary ? 'btn-secondary ' : ''}
        ${this.disabledButton ? 'disabled ' : ''}"
      >
        <slot></slot>
      </button>
    `;
  }

  click() {
    if (!this.disabledButton) {
      const clicked = new CustomEvent('clicked');
      this.dispatchEvent(clicked);
    }
  }
}

customElements.define('app-button', AppButton);
