import { LitElement, html, css } from 'lit-element';
import { navigator } from 'lit-element-router';
import { sharedStyles } from './shared-styles.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icons/social-icons.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';

export class AppLink extends navigator(LitElement) {
  static get properties() {
    return {
      href: { type: String },
      icon: { type: String },
    };
  }

  static get styles() {
    return [
      sharedStyles,
      css`
        :host {
          display: inline-block;
        }
        .link {
          background: var(--theme-link-background);
          padding: 10px 15px;
          height: 25px;
        }
        .link a {
          font-size: 16px;
          color: var(--theme-link-color);
          text-decoration: none;
        }
        :host :hover,
        .link :hover {
          background: var(--theme-link-hover-background);
        }
        :host :hover a,
        .link :hover a {
          color: var(--theme-link-hover-color);
        }
      `,
    ];
  }

  constructor() {
    super();
    this.href = '';
  }

  render() {
    return html`
      <div class="link">
        <a href="${this.href}" @click="${this.linkClick}">
          ${this.icon ? html`<iron-icon icon="${this.icon}"></iron-icon>` : ``}
          <slot></slot>
        </a>
      </div>
    `;
  }

  linkClick(event) {
    event.preventDefault();
    this.navigate(this.href);
  }
}

customElements.define('app-link', AppLink);
